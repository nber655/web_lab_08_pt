"use strict";

// TODO Your code here.

// Declare initial array "raw_array"
// with length = 0
var raw_array = [];
// Maximum number of elements in the array
var max_array_elements = 20;

//Funtion required to sort array numerically and in ascending order
//Return values:
//Less than 0: Sort "a" to be a lower index than "b"
//Zero: "a" and "b" should be considered equal, and no sorting performed.
//Greater than 0: Sort "b" to be a lower index than "a".

function sort_ascending_function(a, b) {
    var sort_ascending = a - b;
    return sort_ascending;
}

//Generate an array of 20 random values between 1 -100 (inclusive)
//Random values can be duplicated
// for (var i = 0; i < 20; i++) {
//     //Generate random value
//     var random_value = Math.floor(Math.random() * 100) + 1;
//     //  Adds a new element ("random_value") to the array "raw_array"
//     raw_array.push(random_value); 
    
// }

for (var i = 0; raw_array.length < max_array_elements; i++) {
    //Generate random value
    var random_value = Math.floor(Math.random() * 100) + 1;
    //  Adds a new element ("random_value") to the array "raw_array"
    raw_array.push(random_value); 
    
}

// while (raw_array.length < 20) {
//     //Generate random value
//     var random_value = Math.floor(Math.random() * 100) + 1;
//     //  Adds a new element ("random_value") to the array "raw_array"
//     raw_array.push(random_value); 
    
// }

// Print array "raw_array" without been sorted
console.log("Raw array is: " + raw_array); 
// Sort array "raw_array" in ascending order
var raw_array_sorted = raw_array.sort(sort_ascending_function); 
// Print sorted array "raw_array" and in ascending order
console.log("Sorted array is: " + raw_array_sorted);
//Number of loop execution 
console.log("Loop executed: " + i + " time");

