"use strict";

// TODO Create recipes in recipe book arrays here

var recipeBook = [];
recipeBook["Banana Cake"] = [
    "250g butter",
    "1 1/2 cups sugar",
    "4 eggs",
    "2 tsp vanilla extract",
    "4 very ripe bananas",
    "2 tsp baking soda",
    "1/2 cup hot milk",
    "3 cups flour",
    "2 tsp baking powde"
  ];
recipeBook["Pound Cake"] = [
    "3 cups flour",
    "2 tsp baking powder",
    "1/2 tsp salt",
    "240g butter",
    "2 cups sugar",
    "3 eggs",
    "1 cup milk",
    "2 tsp vanilla extract"
  ];
recipeBook["Orange Cake"] = [
    "1 whole orange",
    "1 tsp baking soda",
    "1/2 cup water",
    "125g butter",
    "1 cup sugar",
    "2 eggs",
    "1 tsp vanilla extract",
    "2 cups flour",
    "1 cup sultanas or raisins",
    "1/2 cup chopped walnuts"
  ];
recipeBook["Chocolate Cake"] = [
    "1 3/4 cup flour",
    "1/2 cup cocoa",
    "2 tsp baking powder",
    "1 cup sugar",
    "125g butter",
    "2 Tbsp golden syrup",
    "2 eggs",
    "1 1/2 cup milk",
    "2 tsp baking soda",
    "1 tsp vanilla essence"
  ];

// TODO Print recipes included in recipe book
  for( var recipe in recipeBook) {
    console.log(recipe + ":");

    for(var index_recipe = 0; index_recipe < recipeBook[recipe].length; index_recipe++) {
      var ingredients = recipeBook[recipe][index_recipe];
      console.log("-" + " " + ingredients) + ",";
    }
    console.log("\n");
  }

// Instruction for information and illustration purpose only
  // console.log("Number of recipes in recipebook are: " + Object.keys(recipeBook).length);
  // console.log("Number of ingredients in banana_cake recipe are: " + recipeBook["Banana Cake"].length);
  // console.log("Number of ingredients in pound_cake recipe are: " + recipeBook["Pound Cake"].length);
  // console.log("Number of ingredients in orange_cake recipe are: " + recipeBook["Orange Cake"].length);
  // console.log("Number of ingredients in chocolate_cake recipe are: " + recipeBook["Chocolate Cake"].length);