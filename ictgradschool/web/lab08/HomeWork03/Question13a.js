"use strict";

// TODO Create recipes in recipe book arrays here

var banana_cake = [];
banana_cake[0] = "Banana Cake";
banana_cake[1] = "250g butter";
banana_cake[2] = "1 1/2 cups sugar";
banana_cake[3] = "4 eggs";
banana_cake[4] = "2 tsp vanilla extract";
banana_cake[5] = "4 very ripe bananas";
banana_cake[6] = "2 tsp baking soda";
banana_cake[7] = "1/2 cup hot milk";
banana_cake[8] = "3 cups flour";
banana_cake[9] = "2 tsp baking powde";

var pound_cake = [];
pound_cake[0] = "Pound Cake";
pound_cake[1] = "3 cups flour";
pound_cake[2] = "2 tsp baking powder";
pound_cake[3] = "1/2 tsp salt";
pound_cake[4] = "240g butter";
pound_cake[5] = "2 cups sugar";
pound_cake[6] = "3 eggs";
pound_cake[7] = "1 cup milk";
pound_cake[8] = "2 tsp vanilla extract";

var orange_cake = [];
orange_cake[0] = "Orange Cake";
orange_cake[1] = "1 whole orange";
orange_cake[2] = "1 tsp baking soda";
orange_cake[3] = "1/2 cup water";
orange_cake[4] = "125g butter";
orange_cake[5] = "1 cup sugar";
orange_cake[6] = "2 eggs";
orange_cake[7] = "1 tsp vanilla extract";
orange_cake[8] = "2 cups flour";
orange_cake[9] = "1 cup sultanas or raisins";
orange_cake[10] = "1/2 cup chopped walnuts";


var chocolate_cake = [];
chocolate_cake[0] = "Chocolate Cake";
chocolate_cake[1] = "1 3/4 cup flour";
chocolate_cake[2] = "1/2 cup cocoa";
chocolate_cake[3] = "2 tsp baking powder";
chocolate_cake[4] = "1 cup sugar";
chocolate_cake[5] = "125g butter";
chocolate_cake[6] = "2 Tbsp golden syrup";
chocolate_cake[7] = "2 eggs";
chocolate_cake[8] = "1 1/2 cup milk";
chocolate_cake[9] = "2 tsp baking soda";
chocolate_cake[10] = "1 tsp vanilla essence";

// TODO Create recipe book arrays here

var recipeBook = [] ;
recipeBook[0] = banana_cake;
recipeBook[1] = pound_cake;
recipeBook[2] = orange_cake;
recipeBook[3] = chocolate_cake;
 
// TODO Print recipe book recipes here

for(var index_recipebook = 0; index_recipebook < recipeBook.length; index_recipebook++) {
    
    for(var index_recipe = 0; index_recipe < recipeBook[index_recipebook].length; index_recipe++) {
        var ingredients = recipeBook[index_recipebook][index_recipe];
        if (index_recipe == 0) {
            console.log(ingredients + ":"); 
        } 
        else {
            console.log("-" + " " + ingredients) + ",";
        }
        
    }
    console.log(" ");
}


console.log("Number of recipes in recipebook are: " + recipeBook.length);
console.log("Number of ingredients in banana_cake recipe are: " + banana_cake.length);
console.log("Number of ingredients in pound_cake recipe are: " + pound_cake.length);
console.log("Number of ingredients in orange_cake recipe are: " + orange_cake.length);
console.log("Number of ingredients in chocolate_cake recipe are: " + chocolate_cake.length);