"use strict";

  // Question 10

  var array = ["Hello", "World"];
  array[3] = "Bob";
  console.log(array[2]);

  // There is a hole in the array. Index 2 (array[2]) in the array has not been defined.
  // Output will be "undefined" because we are trying to access an element that has not been define in the array.
