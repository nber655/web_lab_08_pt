"use strict";

// Provided variables
var string1 = "Hello World";
var string2 = "Hieverybody!"
var string3 = "Hi, Dr Nick!"

// TODO Your answers here.

var exc301 = "No. Of Character is: " + string1.length;
console.log(exc301);

var exc302 = "The last 3 characters of string1 are: " + string1.substring((string1.length - 3),string1.length); //string1.slice(-3);
console.log(exc302);

var exc303 = "The Space is located at position: " + string2.search(" ");
console.log(exc303);

if(string2.search(" ") >= 0) {
  console.log("There is a space");
} else {
  console.log("There is not space");  
  }

var exc304 = "Last characters of string3 is: " + string3.charAt(string3.length - 1);
console.log(exc304);

// Question 7:
// Given the following variable declaration:
// var myString = "COMPSCI719 is awesome!";
// Write code that prints the following to the console:
// The last eight characters of the string
// The first seven characters, in lower case
// The eighth - 10th characters, converted to an integer, plus the length of the whole string
// In other words, the output should be:
// awesome!
// compsci
// 741

//var last8characters = "The last 8 characters are: " + myString.substring((myString.length - 8), myString.length);
//console.log(myString.slice(-8));

var myString = "COMPSCI719 is awesome!";

// The last eight characters of the string
console.log(myString.slice(myString.length - 8, myString.length));

// The first seven characters, in lower case
console.log(myString.toLowerCase().substring(0, 7));

// The eighth - 10th characters, converted to an integer, plus the length of the whole string
console.log((parseInt(myString.substr(7, 3)) + myString.length));

var v1 = 1 + 2 + "abc";
var v2 = "abc" + 1 + 2;

// Question 8
console.log(v1);
console.log(v2);

// Question 12

//var myAge = 80;
//var myAge = 4;
var myAge = 41;

if (myAge >= 0 && myAge <= 10) {
    console.log("A");
  } else if ((myAge >= 30 && myAge <= 39) || (myAge >= 80 && myAge <= 89)) {
    console.log("B");
  } else {
    console.log("C");
  }

  // Question 11
  //Name: Alice
  //Age: 24
  //Address: Earth
  //Name: Bob
 // Age: 42
  //Address: Jupiter
 // Name: Caitlin
  //Age: 21
  //Address: The Sun

  var personnel = [];
  personnel[0] = ["Alice", 24, "Earth"];
  personnel[1] = ["Bob", 42, "Jupiter"];
  personnel[2] = ["Caitlin", 21, "The Sun"];
  

  for (var i = 0; i < personnel.length; i++) {
    console.log("Name: " + personnel[i][0]);
    console.log("Age: " + personnel[i][1]);
    console.log("Address: " + personnel[i][2]);
  }

  // Question 10

  var array = ["Hello", "World"];
  array[3] = "Bob";
  console.log(array[2]);

  // Output will be "undefined" because we are trying to access an element with non-existent index (array[2]) in the array.
  // array[0] = "Hello";
  // array[1] = "World";
  // array[2] = NOT DECLARED
  // array[3] = "Bod";