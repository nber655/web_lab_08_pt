"use strict";

// HomeWork 4 - Question 9

// The following array has been defined:

// var cars = [
//   {year: 2007, make: "BMW", model: "323i"},
//   {year: 2005, make: "Renault", model: "Megane"},
//   {year: 2008, make: "Jaguar", model: "XF 3.0"}
// ];
// Write JavaScript code which will create an HTML Table in which to display this data, 
// and add the table to the document.body. The table should have three columns: one each for the 
// cars' make, model and year. All three cars should also be displayed in the result table. You don't need to worry about column headings.


  // Create Car array
  var cars = [
    {year: 2007, make: "BMW", model: "323i"},
    {year: 2005, make: "Renault", model: "Megane"},
    {year: 2008, make: "Jaguar", model: "XF 3.0"}
  ]


  function createTable() {
    
        // Check if table already exists. If it does, display an error and quit the function.
        var table = document.getElementById("theTable");
        if (table != null) {
            alert("Table already exists!")
            return;
        }
    
        // Get div where we'll be adding the table
        var div = document.getElementsByClassName("tableContainer")[0];
    
        // Create table
        table = document.createElement("table");
    
        // Set its properties
        table.id = "theTable";
        table.classList.add("table");
        table.classList.add("table-striped");
    
        // Create a header
        var thead = document.createElement("thead");
        var row = document.createElement("tr");
        var cell1 = document.createElement("th");
        cell1.innerHTML = "Column #1";
        var cell2 = document.createElement("th");
        cell2.innerHTML = "Column #2";
    
        // Add cells to row
        row.appendChild(cell1);
        row.appendChild(cell2);
    
        // Add row to thead
        thead.appendChild(row);
    
        // Create a table body
        var tbody = document.createElement("tbody");
        tbody.id = "theTableBody";
    
        // Add thead and tbody to table
        table.appendChild(thead);
        table.appendChild(tbody);
    
        // Add table to div
        div.appendChild(table);
    
    }