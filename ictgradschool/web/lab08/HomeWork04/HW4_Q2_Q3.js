"use strict";

  // HomeWork 4 - Question 2

//   Write a function which takes three numbers as arguments, a, b, and c. The function should return the middle value. For example:
//   If a = 3, b = 4, and c = 5, the function should return 4.
//   If a = 7, b = -2, and c = 10, the function should return 7.
//   Note: The logic for this function is easily achievable using conditional statements. However, for bonus points, 
//   write the function such that no conditional statements are used. This page (Links to an external site.)Links to an external site. 
//   may be of assistance

// int a,b,c;
// int min,mid,max,i=1;

// if(((a<b)&&(b<c)) || ((a>b)&&(a<c)))
// {
//     min=a;
//     mid=b;
//     max=c;
// }
// if(((b<a)&&(a>c)) || ((a>b)&&(a<c)))
// {
//     min=c;
//     mid=a;
//     max=b;
// }
// if(((c<a)&&(c>b)) || ((c>a)&&(c<b)))
// {
//     min=b;
//     mid=c;
//     max=a;
// }


// int main()
// {
//         int a=10,b=11,c=12;
 
// 		//Checking for a is middle number or not
// 		if( b>a && a>c || c>a && a>b )
// 		{
// 			printf("a is middle number");
// 		}
 
//         //Checking for b is middle number or not
// 		if( a>b && b>c || c>b && b>a )
// 		{
// 			printf("b is middle number");
// 		}
 
//         //Checking for c is middle number or not
// 		if( a>c && c>b || b>c && c>a )
// 		{
// 			printf("c is middle number");
// 		}
		
 
// 		return 0;
// }

// int main()
// {
//         int a=10,b=11,c=12;
 
// 		if(a>b)
// 		{
// 			if(b>c)
// 			{
// 				printf("b is middle one");
// 			}
// 			else if(c>a)
// 			{
// 				printf("a is middle one");
// 			}
// 			else
// 			{
// 				printf("c is middle one");
// 			}
// 		}
// 		else
// 		{
// 			if(b<c)
// 			{
// 				printf("b is middle one");
// 			}
// 			else if(c<a)
// 			{
// 				printf("a is middle one");
// 			}
// 			else
// 			{
// 				printf("c is middle one");
// 			}
// 		}
 
// 		return 0;
// }

function middleNumber(n1, n2, n3) {
  var mid = null;
  if( n1 > n2 )   {
    if( n3 > n2) {
      if( n3 < n1 ) {
        mid = n3;
      }
      else {
        mid = n1;
      }                               
    }
    else {
       mid = n2;
    }
  }
 else {
  if( n2 > n3) {
    if( n1 > n3 ) {
      mid = n1;
    }
    else {
      mid = n3;
    }                               
  }
  else {
     mid = n2;
  }
  }
return mid ;
}


function getmiddleNumber(n1, n2, n3) {
var middle = null;
if (n1 > n2) {
  if (n2 > n3) {
    middle = n2;
  } 
  else if (n1 > n3) {
    middle = n3;
  } 
  else {
    middle = n1;
  }
} else {
  if (n1 > n3) {
    middle = n1;
  } 
  else if (n2 > n3) {
    middle = n3;
  } 
  else {
    middle = n2;
  }
}
return middle ;
}

function getmiddleValue(a, b, c) {
  // middleValue = max(min(a,b), min(max(a,b),c));
  var x = Math.min(a,b);
  var y = Math.max(a,b);
  var z = Math.min(y,c);
  var middleValue = Math.max(x, z);
  return middleValue;
}

var number1 = -50;
var number2 = 1;
var number3 = 20;

console.log(middleNumber(number1, number2, number3));
console.log(getmiddleNumber(number1, number2, number3));


  // HomeWork 4 - Question 3
// Three variables have been defined: number1, number2, and number3. 
// Write code which uses your function from question 2 to find the middle of those three numbers, and prints it to the browser console.

console.log(getmiddleValue(number1, number2, number3));

