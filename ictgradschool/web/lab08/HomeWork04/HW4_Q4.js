"use strict";

// HomeWork 4 - Question 4
//   What is the output of the following program? Why?
var text = "Dr. Nick";

function myFunc(text) {
  console.log(text);
  text = "CS719"; // This line modify the value of parameter "text" of the function not the variable "text"
};

console.log(text); // Print Dr. Nick - value of variable "text" of string data type
myFunc("Thomas"); // Print Thomas - value of parameter "text" of function myFunc
console.log(text); // Print Dr. Nick - value of variable "text" of string data type. Function myFunc modify the value of parameter "text" not the variable "text".