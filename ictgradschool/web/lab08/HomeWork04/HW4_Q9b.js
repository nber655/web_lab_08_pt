"use strict";


// HomeWork 4 - Question 9

// The following array has been defined:

// var cars = [
//   {year: 2007, make: "BMW", model: "323i"},
//   {year: 2005, make: "Renault", model: "Megane"},
//   {year: 2008, make: "Jaguar", model: "XF 3.0"}
// ];
// Write JavaScript code which will create an HTML Table in which to display this data, 
// and add the table to the document.body. The table should have three columns: one each for the 
// cars' make, model and year. All three cars should also be displayed in the result table. You don't need to worry about column headings.


  // Create Car array
  var cars = [
    {year: 2007, make: "BMW111", model: "323i"},
    {year: 2005, make: "Renault", model: "Megane"},
    {year: 2008, make: "Jaguar", model: "XF 3.0"}
  ]

function createmyTable() {
     // Using Boostrap framework
    // Create table 
    var mytable = document.createElement("table");
    // Add Boostrap framework classes
    mytable.id = "Table1";
    mytable.classList.add("table");
    mytable.classList.add("test");
    mytable.classList.add("table-striped");
    document.body.appendChild(mytable);

    // // Create table header
    // var mytable_header = document.createElement("thead");
    // var mytable_header_row = document.createElement("tr");
    
    // for(var i = 0; i < 1; i++) {
    //     for(var key in cars[i]) {
    //         var header_cell = document.createElement("th");
    //         header_cell.innerHTML = key;
    //         mytable_header_row.appendChild(header_cell);
    //     }
    // }
    // // Add row to table header
    // mytable_header.appendChild(mytable_header_row);

    // Create a table body, rows and populate each row cell with array data
    // var mytable_body = document.createElement("tbody");
    
    
    for(var cars_array_index in cars) {
        var i = 0;
    //   var mytable_body_row = document.createElement("tr");
        var my_table = document.getElementById("Table1");
        var mytable_row = my_table.insertRow(cars_array_index);
        
      for(var cars_object_key in cars[cars_array_index]) {
        //   var body_cell = document.createElement("td");
        
        var body_cell = mytable_row.insertCell(i);
        body_cell.innerHTML = cars[cars_array_index][cars_object_key];
        // mytable_body_row.appendChild(body_cell);
        i++;
      }
    

    //   mytable_body.appendChild(mytable_body_row);
    }

    // Add table header and body to table
    // mytable.appendChild(mytable_header);
    // mytable.appendChild(mytable_body);
   // add the table to the document.body
    // document.body.appendChild(mytable);
    // // Add table to div in HTML document
    // var tablearea = document.getElementsByClassName("mytablearea");
    // tablearea[0].appendChild(mytable);

}


