"use strict";

// Provided variables
var amount = 3;
var userResponse = 'Y';
var firstName = "Bob";
var singer = "NOT Taylor Swift!";
var year = 1977;

// Example
var isOdd = (amount % 2 != 0);
console.log("isOdd = " + isOdd);

//Exercise Two
if(userResponse == "y" || userResponse == "Y") {
    console.log("exc201_Valid");
}
else {
    console.log("exc201_Invalid");
}

var FirstLetter = firstName.charAt(firstName.length - 1);
console.log(FirstLetter);
if(FirstLetter == "A") {
    console.log("exc202_Valid");
}
else {
    console.log("exc202_Invalid");
}

if(singer == "Taylor Swift") {
    console.log("exc203_Valid");
}
else {
    console.log("exc203_Invalid");
}

if(year > 1978 && year != 2013) {
    console.log("exc204_Valid");
}
else {
    console.log("exc204_Invalid");
}
// TODO write your answers here
