"use strict";

// Provided variables.
var year = 2016;

// Variables you'll be assigning to in this question.
var isLeapYear;

// TODO Your code for part (2) here.
if((year % 4) == 0) {
  console.log("Even");
}else {
    console.log("Odd");
}

if((year % 100) != 0) {
  console.log("Even");
}else {
    console.log("Odd");
}

if((year % 400) == 0) {
  console.log("Even");
}else {
    console.log("Odd");
}

if(((year % 400 == 0) || (year % 100 != 0)) && (year % 4 == 0)) {
  isLeapYear = true;  
} else {
    isLeapYear = false;
}

// function leapYear(year)
// {
//   return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
// }

//isLeapYear = leapYear(year);

// Printing the answer
if (isLeapYear) {
    console.log("Part 2: " + year + " is a leap year.");
} else {
    console.log("Part 2: " + year + " is NOT a leap year.");
}