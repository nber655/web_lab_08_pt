"use strict";

// TODO Create object prototypes here
// Car object constructor function
function Car(year, make, model, bodytype, transmission, odometer, price) {
    this.year = year;
    this.make = make;
    this.model = model;
    this.bodytype = bodytype;
    this.transmission = transmission;
    this.odometer = odometer;
    this.price = price;
    // Testing generate full car detail text message using a method within the object
    // this.getFullCarDetails = function() {
    //     return "- " + this.year + " " + this.make + " " + this.model + ", " + this.odometer + "kms. " + "Asking price: $" + this.price;
    // }
};

// Music Album object constructor function
function MusicAlbum(title, artist, year, genre, tracks) {
    this.title = title;
    this.artist = artist;
    this.year = year;
    this.genre = genre;
    this.tracks = [];
    for (var ind = 0; ind < tracks.length; ind++) {
        this.tracks.push(tracks[ind]);
    }
    
    this.getFullAlbumDetails = function() {
        return "Album: " + "\"" + this.title + "\", released in " + this.year + " by " + this.artist + ".";
    }
};

// TODO Create functions here
// Function to create Cars array
function getCars() {
    // Declare initial array "Cars" and create 3 car objects
    var Cars = [];
    Cars["Car1"] = new Car(2000, "BMW", "323i", "Sedan", "Tictronic", 68512, 16000);
    Cars["Car2"] = new Car(2001, "Toyota", "Rav4", "SUV", "Automatic", 10000, 32000);
    Cars["Car3"] = new Car(2002, "Nissan", "Qashqi", "SUV", "Manual", 20000, 64000);
    
    return Cars;
};

// Function to create Albums array
function getAlbums() {
    // Declare initial array "Albums" and create 3 music album objects
    var Albums = [];
    
    Albums["Album1"] = new MusicAlbum("1989", "Taylor Swift", 2014, "Pop", [
        "Welcome to New York",
        "Blank Space",
        "Style",
        "Out of the Woods",
        "All You Had to Do Was Stay",
        "Shake It Off",
        "I Wish You Would",
        "Bad Blood",
        "Wildest Dreams",
        "How You Get the Girl",
        "This Love",
        "I Know Places",
        "Clean"
        ]);

    Albums["Album2"] = new MusicAlbum("Believe", "Justin Bieber", 2012, "Pop", [
        "All Around the World",
        "Boyfriend",
        "As Long as You Love Me",
        "Catching Feelings",
        "Take You",
        "Right Here",
        "Fall",
        "Die in Your Arms",	
        "Thought of You",	
        "Beauty and a Beat",	
        "One Love",	
        "Be Alright",	
        "Believe"
    ]);

    Albums["Album3"] = new MusicAlbum("Songs of Innocence", "U2", 2014, "Rock", [
        "The Miracle (Of Joey Ramone)",
        "Every Breaking Wave",	    
        "California (There Is No End to Love)",    
        "Song for Someone",	    
        "Iris (Hold Me Close)",	    
        "Volcano",    
        "Raised by Wolves",	
        "Cedarwood Road",	
        "Sleep Like a Baby Tonight",
        "This Is Where You Can Reach Me Now",
        "The Troubles"
    ]);

    return Albums
};

// Function to print Cars array
function printCars(cars_array) {
    console.log("Cars:");
    for (var propCar in cars_array) {
        // Generate full car detail text message by calling each object's key individually.
        console.log("- " + cars_array[propCar].year + " " + cars_array[propCar].make + " " + cars_array[propCar].model + ", " + cars_array[propCar].odometer + "kms. " + "Asking price: $" + cars_array[propCar].price);
        
        // Testing generate full car detail text message byusing the method getFullCarDetails() created in the function Car() to create object prototypes
        // console.log(cars_array[propCar].getFullCarDetails());
    
    }
};

// Funtion to print music albums array
function printAlbums(albums_array) {
    console.log("Albums:");
    for (var propAlbum in albums_array) {
        console.log(albums_array[propAlbum].getFullAlbumDetails() + " Track listing: ");
        for (var index_trackListing = 0; index_trackListing < albums_array[propAlbum].tracks.length; index_trackListing++) {
            var trackList = albums_array[propAlbum].tracks[index_trackListing];
            console.log("-" + " " + trackList);
        }
    }   
    

};

// TODO Complete the program here
// Get and print cars 
printCars(getCars());
console.log("\n");
// Get and print music albums
printAlbums(getAlbums());



