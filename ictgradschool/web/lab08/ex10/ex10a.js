"use strict";

// TODO Create object prototypes here
// Car object constructor function
function Car(year, make, model, bodytype, transmission, odometer, price) {
    this.year = year;
    this.make = make;
    this.model = model;
    this.bodytype = bodytype;
    this.transmission = transmission;
    this.odometer = odometer;
    this.price = price;
    this.getFullCarDetails = function() {
        return "- " + this.year + " " + this.make + " " + this.model + ", " + this.odometer + "kms. " + "Asking price: $" + this.price;
    }
};

// Music Album object constructor function
function MusicAlbum(title, artist, year, genre, tracks) {
    this.title = title;
    this.artist = artist;
    this.year = year;
    this.genre = genre;
    this.tracks = [];
    for (var ind = 0; ind < tracks.length; ind++) {
        this.tracks.push(tracks[ind]);
    }
    
    this.getFullAlbumDetails = function() {
        return "Album: " + "\"" + this.title + "\", released in " + this.year + " by " + this.artist + ".";
    }
};

// TODO Create functions here
// Function to create Cars array
function getCars() {
    // Declare initial array "Cars" and create 3 car objects
    var Cars = [];
    Cars[0] = new Car(2000, "BMW", "323i", "Sedan", "Tictronic", 68512, 16000);
    Cars[1] = new Car(2001, "Toyota", "Rav4", "SUV", "Automatic", 10000, 32000);
    Cars[2] = new Car(2002, "Nissan", "Qashqi", "SUV", "Manual", 20000, 64000);
    
    // Declare initial array "Cars" with length = 0
    // var Cars = [];
    //Generate an array of 3 cars objects
    // for (var i = 0; i < 3; i++) {
    //     //Generate car object
    //     var CarX = new Car(2007, i, "323i", "Sedan", "Tictronic", 68512, 16000);
    //     //  Adds a new element ("CarX") to the array "Car"
    //     Cars.push(CarX); 
    // }
    return Cars;
};

// Function to create Albums array
function getAlbums() {
    // Declare initial array "ALbums" and create 3 music album objects
    var Albums = [];
    // var Album0Tracks = [
    //     "Welcome to New York",
    //     "Blank Space",
    //     "Style",
    //     "Out of the Woods",
    //     "All You Had to Do Was Stay",
    //     "Shake It Off",
    //     "I Wish You Would",
    //     "Bad Blood",
    //     "Wildest Dreams",
    //     "How You Get the Girl",
    //     "This Love",
    //     "I Know Places",
    //     "Clean"
    //     ];
    
    Albums[0] = new MusicAlbum("1989", "Taylor Swift", 2014, "Pop", [
        "Welcome to New York",
        "Blank Space",
        "Style",
        "Out of the Woods",
        "All You Had to Do Was Stay",
        "Shake It Off",
        "I Wish You Would",
        "Bad Blood",
        "Wildest Dreams",
        "How You Get the Girl",
        "This Love",
        "I Know Places",
        "Clean"
        ]);
    // var Album1Tracks = [
    //     "All Around the World",
    //     "Boyfriend",
    //     "As Long as You Love Me",
    //     "Catching Feelings",
    //     "Take You",
    //     "Right Here",
    //     "Fall",
    //     "Die in Your Arms",	
    //     "Thought of You",	
    //     "Beauty and a Beat",	
    //     "One Love",	
    //     "Be Alright",	
    //     "Believe"
    // ];
    Albums[1] = new MusicAlbum("Believe", "Justin Bieber", 2012, "Pop", [
        "All Around the World",
        "Boyfriend",
        "As Long as You Love Me",
        "Catching Feelings",
        "Take You",
        "Right Here",
        "Fall",
        "Die in Your Arms",	
        "Thought of You",	
        "Beauty and a Beat",	
        "One Love",	
        "Be Alright",	
        "Believe"
    ]);
    // var Album2Tracks = [
    //     "The Miracle (Of Joey Ramone)",
    //     "Every Breaking Wave",	    
    //     "California (There Is No End to Love)",    
    //     "Song for Someone",	    
    //     "Iris (Hold Me Close)",	    
    //     "Volcano",    
    //     "Raised by Wolves",	
    //     "Cedarwood Road",	
    //     "Sleep Like a Baby Tonight",
    //     "This Is Where You Can Reach Me Now",
    //     "The Troubles"
    // ];
    Albums[2] = new MusicAlbum("Songs of Innocence", "U2", 2014, "Rock", [
        "The Miracle (Of Joey Ramone)",
        "Every Breaking Wave",	    
        "California (There Is No End to Love)",    
        "Song for Someone",	    
        "Iris (Hold Me Close)",	    
        "Volcano",    
        "Raised by Wolves",	
        "Cedarwood Road",	
        "Sleep Like a Baby Tonight",
        "This Is Where You Can Reach Me Now",
        "The Troubles"
    ]);

    return Albums
};

// Function to find the index of a key in an object
function getObjectKeyIndex(objName, keyToFind) {
    var index = 0, key;

    for (key in objName) {
        if (key == keyToFind) {
            return index;
        }
        index++;
    }
    return null; // Returns null if the key doesn't exist
};

// Function to print Cars array
function printCars(cars_array) {
    var FullCarDetail = ""; 
    console.log("Cars:");
    for (var i = 0; i < cars_array.length; i++) {
        // console.log(cars_array[i].getFullCarDetails());
        // var obj = cars_array[i];
        for (var property in cars_array[i]) {
            var proValue = cars_array[i][property];
            // var count = getObjectKeyIndex(cars_array[i], property);
            // Generate Full Detail Text message: "- year make model, odometerkms. Asking price: $price
            if( (getObjectKeyIndex(cars_array[i], property) < 2)) {
                FullCarDetail += (proValue + " ");
            } 
            else if((getObjectKeyIndex(cars_array[i], property) >= 5) && (getObjectKeyIndex(cars_array[i], property) < 6)) {
                FullCarDetail += (", " + proValue + "kms. " + "Asking price: $" );
            } 
            else if ((getObjectKeyIndex(cars_array[i], property) == 6) || (getObjectKeyIndex(cars_array[i], property) == 2)) {
                FullCarDetail += (proValue);
            }               
        }
        console.log("- " + FullCarDetail);
        // Clear FullCarDetail variable before  continue with next object
        FullCarDetail = "";
        
    }
};

// Funtion to print music albums array
function printAlbums(albums_array) {
    // var FullAlbumsDetail = ""; 
    console.log("Albums:");
    for (var i = 0; i < albums_array.length; i++) {
        console.log(albums_array[i].getFullAlbumDetails() + " Track listing: ");
        for (var index_trackListing = 0; index_trackListing < albums_array[i].tracks.length; index_trackListing++) {
            var trackList = albums_array[i].tracks[index_trackListing];
            // console.log(albums_array[i].tracks.length);
            // console.log(index_trackListing);
            console.log("-" + " " + trackList);
        }
        console.log("\n");     
    }   
    

};

// TODO Complete the program here
// Print cars 
printCars(getCars());
console.log("\n");
//Print music albums
printAlbums(getAlbums());

// **********************FUNCTION TESTING AND VALIDATIONS *******************************
// console.log(arraydetail[0]);
// console.log(arraydetail[1]);
// console.log(arraydetail[2]);
// console.log(getAlbums()[0]);
// console.log(getAlbums()[1]);
// console.log(getAlbums()[2]);

// Creating object
// var myCar = new Car(2007, "BMW", "323i", "Sedan", "Tictronic", 68512, 16000);

// var AlbumTracks = [
//     "Welcome to New York",
//     "Blank Space",
//     "Style",
//     "Out of the Woods",
//     "All You Had to Do Was Stay",
//     "Shake It Off",
//     "I Wish You Would",
//     "Bad Blood",
//     "Wildest Dreams",
//     "How You Get the Girl",
//     "This Love",
//     "I Know Places",
//     "Clean"
//     ];

// var myAlbum = new MusicAlbum("1989", "Taylor Swift", 2014, "Pop", AlbumTracks);

// console.log(myCar);
// console.log(myCar.getFullCarDetails());


// console.log(myAlbum);
// console.log(myAlbum.getFullAlbumDetails());
// console.log(myAlbum.tracks);

// var recipeBook = [];
// recipeBook["Banana Cake"] = [
//     "250g butter",
//     "1 1/2 cups sugar",
//     "4 eggs",
//     "2 tsp vanilla extract",
//     "4 very ripe bananas",
//     "2 tsp baking soda",
//     "1/2 cup hot milk",
//     "3 cups flour",
//     "2 tsp baking powde"
//   ];

