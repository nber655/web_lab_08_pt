"use strict";

// TODO Car
var car = {
    Year: "2007",
    Make: "BMW",
    Model: "323i",
    Bodytype: "Sedan",
    Transmission: "Tiptronic",
    Odometer: "68,512",
    Price: "$16,000"
};


// TODO Music
var musicAlbum = {
    Title: "1989",
    Artist: "Taylor Swift",
    Year: "2014",
    Genre: "Pop",
    Tracks: [
    "Welcome to New York",
    "Blank Space",
    "Style",
    "Out of the Woods",
    "All You Had to Do Was Stay",
    "Shake It Off",
    "I Wish You Would",
    "Bad Blood",
    "Wildest Dreams",
    "How You Get the Girl",
    "This Love",
    "I Know Places",
    "Clean"
    ],
    //Creating the toString() method inside object musicAlbum
    toString: function() {
        // var albun = "Album: " + "\"" + this.Title + "\", released in " + this.Year + " by " + this.Artist;
        // return albun;
        return "Album: " + "\"" + this.Title + "\", released in " + this.Year + " by " + this.Artist;
    }
    };
    
    
    console.log(car);
    console.log(musicAlbum.toString()); // Calling the method - objectName.methodName()

    


