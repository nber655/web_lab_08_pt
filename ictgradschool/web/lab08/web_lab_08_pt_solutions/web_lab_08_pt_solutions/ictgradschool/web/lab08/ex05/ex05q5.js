"use strict";

// Provided variable
var theString = "Hello World!";

// Variable you'll be modifying
var reversed = "";

// TODO Your answer here.
for (var i = 0; i < theString.length; i++) {
    reversed = theString.charAt(i) + reversed;
}

// Printing the answer.
console.log("\"" + theString + "\", reversed, is: \"" + reversed + "\".");

var numbers = [-9, 2, 7, 5, 124, -5, 1, 144];