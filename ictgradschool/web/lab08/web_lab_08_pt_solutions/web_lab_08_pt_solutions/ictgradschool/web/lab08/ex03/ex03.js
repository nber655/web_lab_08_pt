"use strict";

// Provided variables
var string1 = "Hello World";
var string2 = "Hi everybody!"
var string3 = "Hi, Dr Nick!"

// TODO Your answers here.

console.log("1. string1 contains " + string1.length + " characters.");
console.log("2. required substring: " + string1.substring(string1.length - 3, string1.length));
console.log("3. Does string2 contain spaces? " + (string2.indexOf(" ") >= 0));
console.log("4. last character in string3 is: " + string3.charAt(string3.length - 1));