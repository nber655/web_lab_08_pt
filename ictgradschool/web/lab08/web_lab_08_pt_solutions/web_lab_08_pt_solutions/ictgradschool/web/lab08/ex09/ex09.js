"use strict";

// TODO Car
var car = {
    year: 2007,
    make: "BMW",
    model: "323i",
    bodyType: "Sedan",
    transmission: "Tiptronic",
    odometer: 68512,
    price: 16000
};

// TODO Music
var musicAlbum = {
    title: "1989",
    artist: "Taylor Swift",
    year: 2014,
    genre: "Pop",
    tracks: [
        "Welcome to New York",
        "Blank Space",
        "Style",
        "Out of the Woods",
        "All You Had to Do Was Stay",
        "Shake It Off",
        "I Wish You Would",
        "Bad Blood",
        "Wildest Dreams",
        "How You Get The Girl",
        "This Love",
        "I Know Places",
        "Clean"
    ],
    toString: function() {
        return "Album: \"" + this.title + "\", released in " + this.year + " by " + this.artist;
    }
};

// Testing:
console.log(car.bodyType);
console.log(musicAlbum.toString());
console.log(musicAlbum.tracks.length);