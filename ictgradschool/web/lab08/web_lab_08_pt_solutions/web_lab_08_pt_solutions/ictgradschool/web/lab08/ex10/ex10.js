"use strict";

// TODO Create object prototypes here
function Car(year, make, model, bodyType, transmission, odometer, price) {
    this.year = year;
    this.make = make;
    this.model = model;
    this.bodyType = bodyType;
    this.transmission = transmission;
    this.odometer = odometer;
    this.price = price;
}

function MusicAlbum(title, artist, year, genre, tracks) {
    this.title = title;
    this.artist = artist;
    this.year = year;
    this.genre = genre;
    this.tracks = tracks;
    this.toString = function() {
        return "Album: \"" + this.title + "\", released in " + this.year + " by " + this.artist;
    };
}

// TODO Create functions here
function getCars() {
    var cars = [];

    cars[0] = new Car(2007, "BMW", "323i", "Sedan", "Tiptronic", 68512, 16000);

    cars[1] = new Car(2005, "Renault", "Megane", "Convertible", "Automatic", 106369, 6990);

    cars[2] = new Car(2008, "Jaguar", "XF 3.0", "Sedan", "Automatic", 61500, 21980);

    return cars;
}

function getAlbums() {
    var albums = [];

    var tracks0 = ["Welcome to New York", "Blank Space", "Style", "Out of the Woods", "All You Had to Do Was Stay", "Shake It Off",
        "I Wish You Would", "Bad Blood", "Wildest Dreams", "How You Get The Girl", "This Love", "I Know Places", "Clean"];
    albums[0] = new MusicAlbum("1989", "Taylor Swift", 2014, "Pop", tracks0);

    var tracks1 = ["Bohemian Rhapsody", "Imagine", "Boogie Woogie Bugle Boy", "Over the Rainbow", "Take On Me", "Can't Help Falling In Love", "Jolene"];
    albums[1] = new MusicAlbum("PTX Vol. IV - Classics", "Pentatonix", 2017, "Acapella", tracks1);

    var tracks2 = ["Let It Rock", "You Give Love A Bad Name", "Livin' On A Prayer", "Social Disease", "Wanted Dead Or Alive",
        "Raise Your Hands", "Without Love", "I'd Die For You", "Never Say Goodbye", "Wild In The Streets"];
    albums[2] = new MusicAlbum("Slippery When Wet", "Bon Jovi", 1986, "Rock", tracks2);

    return albums;
}

function printCars(cars) {
    console.log("Cars:");
    for (var car of cars) {
        console.log("- " + car.year + " " + car.make + " " + car.model + ", " + car.odometer + "kms. Asking price: $" + car.price);
    }
}

function printAlbums(albums) {
    console.log("Albums:");
    for (var album of albums) {
        console.log("- " + album.toString() + ". Track listing:");
        for (var i = 0; i < album.tracks.length; i++) {
            console.log("  - " + album.tracks[i]);
        }
    }
}

// TODO Complete the program here
var cars = getCars();
var albums = getAlbums();
printCars(cars);
console.log();
printAlbums(albums);