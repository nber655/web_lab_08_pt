"use strict";

var n = 2; // Since 1 is odd we can just start from 2
var sum = 0;
var count = 0;
while (n <= 20) {
    sum += n;
    count ++;
    n += 2; // Incrementing n by 2 each time means we'll only be looking at the even numbers.
}

var avg = sum / count;
console.log("Average = " + avg);