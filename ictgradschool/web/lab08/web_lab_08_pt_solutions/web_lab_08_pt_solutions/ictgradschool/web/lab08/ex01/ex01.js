"use strict";

// TODO Declare variables here
var myFirstNumber = 3;
var myFirstBoolean = false;
var mySecondNumber = 10.25;
var myFirstString = "Hello";
var mySecondString = "World";

var sum = myFirstNumber + 7;
var difference = mySecondNumber - myFirstNumber;
var helloWorld = myFirstString + " " + mySecondString;
var concat = myFirstString + myFirstNumber;
var weird = mySecondString - mySecondNumber;

// TODO Use console.log statements to print the values of the variables to the command line.
console.log("myFirstNumber = " + myFirstNumber);
console.log("myFirstBoolean = " + myFirstBoolean);
console.log("mySecondNumber = " + mySecondNumber);
console.log("myFirstString = \"" + myFirstString + "\"");
console.log("mySecondString = \"" + mySecondString + "\"");

console.log("myFirstNumber + 7 = " + sum);
console.log("mySecondNumber - myFirst number = " + difference);
console.log("myFirstString + mySecondString = " + helloWorld);
console.log("myFirstString + myFirstNumber = " + concat);
console.log("mySecondString - mySecondNumber = " + weird);