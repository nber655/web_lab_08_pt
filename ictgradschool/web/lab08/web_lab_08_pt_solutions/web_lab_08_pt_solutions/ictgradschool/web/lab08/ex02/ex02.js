"use strict";

// Provided variables
var amount = 3;
var userResponse = 'Y';
var firstName = "Bob";
var singer = "NOT Taylor Swift!";
var year = 1977;

// Example
var isOdd = (amount % 2 != 0);
console.log("isOdd = " + isOdd);

// TODO write your answers here

var isYes = (userResponse == 'y' || userResponse == 'Y');
console.log("isYes = " + isYes);

var startsWithA = (firstName.charAt(0) == 'A');
console.log("startsWithA = " + startsWithA);

var isTSwizzle = (singer == "Taylor Swift");
console.log("isTSwizzle = " + isTSwizzle);

var isCorrectYear = (year > 1978 && year != 2013);
console.log("isCorrectYear = " + isCorrectYear);