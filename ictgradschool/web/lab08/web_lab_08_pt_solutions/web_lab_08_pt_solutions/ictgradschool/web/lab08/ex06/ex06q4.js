"use strict";

// TODO Your code here.

// Function to create a random value (obtained from W3Schools website)
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

// Create array
var theArray = [];

// Fill with 30 random values from 1 - 100
for (var i = 0; i < 20; i++) {
    theArray[theArray.length] = getRndInteger(1, 100);
}

// Print it out
console.log("Unsorted: " + theArray.toString());

// Sort it and print it again
theArray.sort(function(a, b){return a - b});
console.log("Sorted: " + theArray.toString());