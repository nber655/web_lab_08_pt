"use strict";

var sum = 0;
var count = 0;

for (var n = 2; n <= 20; n+=2) {
    sum += n;
    count ++;
}

var avg = sum / count;
console.log("Average = " + avg);