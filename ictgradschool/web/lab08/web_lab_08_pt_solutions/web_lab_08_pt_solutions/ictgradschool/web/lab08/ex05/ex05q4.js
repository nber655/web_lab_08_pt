"use strict";

// One way of doing it...
for (var n = 1; n <= 10; n++) {

    var result = 1;
    for (var i = n; i > 1; i--) {
        result *= i;
    }

    console.log(n + "! = " + result);

}

console.log();

// Another, faster way of doing it...
var result = 1;
for (var n = 1; n <= 10; n++) {

    result *= n;
    console.log(n + "! = " + result);
}