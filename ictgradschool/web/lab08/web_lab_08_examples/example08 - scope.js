"use strict";

function scopeDemo1(myInt) {
    myInt = 5;
}

// When we pass value into the function, it will be copied into myInt.

// myInt and value are still two different variables, so assigning
// a different value to myInt doesn’t affect value at all.

var value = 3;
scopeDemo1(value);
console.log(value); // Hence: this prints "3".

// This will still print 3. This version of myInt is a GLOBAL variable,
// while the myInt argument in scopeDemo1 is a different, LOCAL variable.
var myInt = 3;
scopeDemo1(myInt);
console.log(myInt);




// Defining the global variable, aGlobalVariable
var aGlobalVariable = "I'm globetrotter!";

// Function arguments, such as anArgument here, count as local variables.
function scopeDemo2(anArgument) {

    // Defining the local variable, aLocalVariable
    var aLocalVariable = "I'm a local!";

    // We can access all global variables from within this function,
    // as well as all of this function's own local variables.
    // So all these lines are fine.
    console.log(aGlobalVariable);
    console.log(aLocalVariable);
    console.log(anArgument);
}

scopeDemo2("I'm argumentative!");

// We can access all global variables from outside functions, but we
// can't access any local variables. So the two commented-out lines
// won't work (try uncommenting them and see for yourself what happens!)
console.log(aGlobalVariable);
// console.log(aLocalVariable);
// console.log(anArgument);




// When variables have the same name, the code will use the "most local"
// version it can. So, in the following example...

var myVariable1 = "Hello";
var myVariable2 = "World";

function scopeDemo3(myVariable1) {

    var myVariable2 = "Orbis Terrarum"

    // ... This line will use the local versions of myVariable1 and
    // myVariable2, since it can see them (they are defined within this function)
    console.log(myVariable1 + " " + myVariable2);
}

// ... But this line will use the global versions since that's all
// it can see.
// So, this line will print "Hello World..."
console.log(myVariable1 + " " + myVariable2);

// And this line will print "Salve Orbis Terrarum".
scopeDemo3("Salve");