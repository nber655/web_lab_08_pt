"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)
function getRndInteger(min, max) {
    var random_integer = Math.floor(Math.random() * (max - min + 1)) + min;
    return random_integer;
    
}

// TODO Complete this function, which should round the given number to 2dp and return the result.
function roundTo2dp(number) {
    var rounded_number = Math.round((number * 100)) / 100; 
    return rounded_number ;
}

// TODO Write a function which calculates and returns the volume of a cone.
function coneVolume(r1, h1) {
    var cone_volume = ((Math.pow(r1, 2)) * Math.PI * (h1 / 3.0)); 
    return cone_volume ;
}

// TODO Write a function which calculates and returns the volume of a cylinder.
function cylinderVolume(r2, h2) {
    var cylinder_volume = ((Math.pow(r2, 2)) * Math.PI * (h2)); 
    return cylinder_volume ;
}

// TODO Write a function which prints the name and volume of a shape, to 2dp.
function shapeVolume(shape, volume) {
    return console.log("The volume of the " + shape + " is: " + roundTo2dp(volume) + " cm^3"); 

}

// Funtion to print the shape with the largest volume
function largestShape(shape_name, shape_volume) {
    return console.log("The shape with the largest volume is " + shape_name + ", with a volume of " + roundTo2dp(shape_volume) + " cm^3"); 
}

// ------------------------------------------

// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.

// Shape name
var cone_name = "cone";
var cylinder_name = "cylinder";
// Ex08.01 - Randomly generate the radius and height of a cone and a cylinder 
//Radius and height limits
var lowerLimit = 25;
var upperLimit = 50;
// Radius and height of a cone
var cone_r = getRndInteger(lowerLimit,upperLimit);
var cone_h = getRndInteger(lowerLimit,upperLimit);

// Radius and height of a cone
var cylinder_r = getRndInteger(lowerLimit,upperLimit);
var cylinder_h = getRndInteger(lowerLimit,upperLimit);

//Ex08.02 - Calculate cone and cylinder volume
//Cone Volume
var cone_volume = roundTo2dp(coneVolume(cone_r, cone_h));

//Cylinder Volume
var cylinder_volume = roundTo2dp(cylinderVolume(cylinder_r, cylinder_h));

//Ex08.03 - Print the volume of both shapes
shapeVolume(cone_name, cone_volume);
shapeVolume(cylinder_name, cylinder_volume);

//Ex08.04 - Determine the larger of the two volumes
var larger_volume = roundTo2dp(Math.max(cone_volume, cylinder_volume));

// Ex08.05 - Print the name and volume of the largest shape
if (larger_volume == cone_volume) {
    largestShape(cone_name, cone_volume);
}    
else if (larger_volume == cylinder_volume) {
    largestShape(cylinder_name, cylinder_volume);
    
}

