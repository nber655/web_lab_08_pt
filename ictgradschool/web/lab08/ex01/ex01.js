"use strict";

// TODO Declare variables here
// Number
var myFirstNumber = 3;
//Boolean
var myFirstBoolean = true;
//Floating Point
var str = "10.25";
var mySecondNumber = parseFloat(str);
//String
var myFirstString = "Hello";
var mySecondString = "World";

// Variable manipulation
var exc106 = myFirstNumber + 7;
var exc107 = mySecondNumber - myFirstNumber;
var exc108 = myFirstString + " " + mySecondString;
var exc109 = myFirstString + myFirstNumber;
var exc110 = mySecondString - mySecondNumber;

// Console print out
console.log("myFirstNumber = " + myFirstNumber);
console.log("mySecondNumber - myFirstNumber = " + exc107);
console.log("myFirstString + mySecondString = " + exc108);
console.log("myFirstString + myFirstNumber = " + exc109);
console.log("mySecondString - mySecondNumber = " + exc110);
console.log("\"Hello World\"");

// TODO Use console.log statements to print the values of the variables to the command line.

// Question 6 
var myInt1 = 5;
var myInt2 = 8;
var mySum = (myInt1 + myInt2);
console.log("Total sum is: " + mySum);
